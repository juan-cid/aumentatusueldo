# Calculadoras
Repositorio creado para mantener todo el codigo necesario para el desarrollo de  las calculadoras Aumenta Tu Sueldo, APV y Cual AFP te conviene, para AFP Modelo. 


# Resumen
Esta aplicación es una conversión de la estructura monolitica del actual aplicativo web Aumenta Tu Sueldo, por una estructura de microservicios, además de desarrollar nuevas funcionalidades para mejorar tanto la experiencia de usuarios externo como usuarios internos. Para estos ultimos se dispondrá una aplicación cliente web exclusiva para el desarrollo de sus actividades.


# Estructura de la solución
Se realizo una estructura que busca separar los codigos dedicados a Infraestructura e Integración Continua de aquellos perteneciente al desarrollo de microservicios, permitiendo así al equipo desarrollar de forma independiente y ordenada cada uno de estos.

Actualmente se tienen divididas las carpetas en tres grandes grupos(esto puede cambiar), los que son los siguientes:
- Build: carpeta dedicada a la construcción del aplicativo en un entorno DevOps, ej. Gitlab, las cuales deben ser separadas y escritas según lo requiera el entorno.

- Deploy: carpeta dedicada al deploy del aplicativo en kubernetes, el cual debe contener los manifiestos necesarios para su ejecución.

- Src: carpeta dedicada a la construcción del aplicativo en su totalidad, el cual se divide en los siguientes sub grupos:
     - ApiGateways: carpeta dedicada a los back-end que se utilizarán de intermediarios entre el Front-End y los microservicios.
     
     - FrontEnd: carpeta dedicada a los front-end necesarios para que los usuarios puedan trabajar con los servicios.
     
     - Services: carpeta dedicada a los microservicios necesarios para la aplicación.


# Como solicitar acceso al Repositorio

Para solicitar acceso al repositorio debe enviar un correo a juan.cid@ext.sonda.com
donde se debe indicar que permisos necesita:

 - Lectura, ideal para QA, Revisores de Codigo, Scrum Master, Product Owner.
 - Escritura, ideal para desarrolladores.


# Como clonar el codigo.

Para clonar el codigo es necesario crear previamente una clave ssh


## WIP 