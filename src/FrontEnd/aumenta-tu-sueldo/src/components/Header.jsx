import React from 'react';
import logo from '../assets/img/logo.png';

const Header = () => (
  <div className="container-fluid">
    <div className="col-12 col-md-12">
      <nav className="navbar fixed-top navbar-dark bg-dark">
        <a
          className="navbar-brand mx-auto"
          href="https://www.aumentatusueldo.cl/"
        >
          <img src={logo} alt="logo-afp-modelo" />
        </a>
      </nav>
    </div>
  </div>
);

export default Header;
