import React from 'react';
import '../assets/styles/Footer.css';

const Footer = () => (
  <footer className="footer">
    <p className="disclaimer text-white text-justify">
      Variación de sueldo líquido aproximada antes de impuestos, sin asignación de
      colación y movilización.Tabla de impuestos de segunda categoría a septiembre de 2020.
      &#34;La rentabilidad
      es variable, por lo que nada garantiza que las rentabilidades pasadas se repitan en el futuro.
      AFP Modelo no se hace cargo de la veracidad de los datos que ingrese el usuario a esta calculadora,
      y este autoriza a recibir información relacionada a AFP Modelo al completar
      los datos solicitados. Infórmese sobre la rentabilidad de su Fondo de Pensiones, las comisiones y la calidad de
      servicio de las AFP en el sitio web de la Superintendencia de Pensiones: www.spensiones.cl&#34;. Estructura de
      comisiones por depósitos de cotizaciones vigentes a octubre de 2020 - Capital: 1,44%, Cuprum: 1,44%, Habitat:
      1 ,27%, Modelo: 0,77%, Planvital: 1,16%, Provida: 1,45%, Uno 0,69% - Para afiliados dependientes, independientes y
      voluntarios. Fuente: Superintendencia de Pensiones.
    </p>
  </footer>
);

export default Footer;
