import React, { useEffect } from 'react';
import Header from '../components/Header';
import Footer from '../components/Footer';
import '../assets/styles/Calculadora.css';
import ahorro from '../assets/svg/ahorro.svg';
import pareja from '../assets/svg/couple.svg';
import useForm from '../utils/useForm';

export default function Calculadora() {
  function submit() {
    console.log('submiting form ...');
  }

  const { handleChange, handleSubmit, form } = useForm(submit);

  useEffect(() => {
  });

  return (
    <div>
      <Header />
      <section>
        <div className="row w-100 container-padre">
          <div className="col-12 d-flex justify-content-center mobile">
            <div className="container-title">
              <h1>Conoce cuánto aumentaría tu sueldo, al cambiarte</h1>
              <h1>a AFP Modelo.</h1>
              <p>Únete a la AFP con una de las comisiones más bajas del mercado.</p>
              <img src={ahorro} alt="cerditoAhorro" />
            </div>
          </div>
          <div className="col-sm-12 col-md-5 bloque desktop">
            <div className="txtDesktop">
              <div className="container-title">
                <h1>Cambiate a Modelo,</h1>
                <h1>paga una menor</h1>
                <h1>comisión,</h1>
                <h1 className="blue">y ahorra todos</h1>
                <h1 className="blue">los meses.</h1>
                <p>Únete a la AFP con una de las comisiones más bajas del mercado.</p>
              </div>
            </div>
          </div>
          <div className="col-sm-12 col-md-2 bloque desktop">
            <img src={pareja} alt="pareja modelo" />
          </div>
          <div className="col-sm-12  col-md-5 bloque form">
            <div className="card">
              <div className="card-body">
                <form onSubmit={handleSubmit}>
                  <div className="form-group input-wrapper">
                    <p>Ingresa tus datos:</p>
                    <input
                      type="text"
                      className="form-control form-control-lg"
                      id="nombre"
                      name="nombre"
                      aria-describedby="nombreAyuda"
                      placeholder="Nombre"
                      onChange={handleChange}
                      value={form.nombre}
                      required
                    />
                    <label
                      htmlFor="nombre"
                      className="control-label"
                    >
                      Nombre
                    </label>
                    <small id="nombreAyuda" className="form-text text-muted">
                      Indicanos tu nombre
                    </small>
                  </div>
                  <div className="form-group input-wrapper">
                    <input
                      type="text"
                      className="form-control form-control-lg"
                      id="rut"
                      name="rut"
                      aria-describedby="rutAyuda"
                      placeholder="RUT"
                      onChange={handleChange}
                      required
                    />
                    <label
                      htmlFor="rut"
                      className="control-label"
                    >
                      RUT
                    </label>
                    <small id="rutAyuda" className="form-text text-muted">
                      Ej.
                      12.345.678-9
                    </small>
                  </div>
                  <div className="form-group input-wrapper">
                    <input
                      type="email"
                      className="form-control form-control-lg"
                      id="correo"
                      value={form.correo}
                      name="correo"
                      aria-describedby="correoAyuda"
                      placeholder="Correo"
                      onChange={handleChange}
                      required
                    />
                    <label
                      htmlFor="correo"
                      className="control-label"
                    >
                      Correo
                    </label>
                    <small id="correoAyuda" className="form-text text-muted">
                      Ej.
                      aumentatusueldo@afpmodelo.cl
                    </small>
                  </div>
                  <div className="form-group input-wrapper">
                    <input
                      type="text"
                      className="form-control form-control-lg"
                      id="celular"
                      name="celular"
                      aria-describedby="celularAyuda"
                      placeholder="Celular"
                      value={form.celular}
                      onChange={handleChange}
                      pattern="^[0-9]{9}$"
                      required
                    />
                    <label
                      htmlFor="celular"
                      className="control-label"
                    >
                      Celular
                    </label>
                    <small id="celularAyuda" className="form-text text-muted">
                      Ej.
                      9111223XX
                    </small>
                  </div>
                  <div className="form-group input-wrapper">
                    <input
                      type="text"
                      className="form-control form-control-lg"
                      id="sueldo"
                      name="sueldo"
                      aria-describedby="sueldoAyuda"
                      placeholder="Sueldo Liquido"
                      value={form.sueldo}
                      onChange={handleChange}
                      pattern="^[0-9]{6,9}$"
                      required
                    />
                    <label
                      htmlFor="sueldo"
                      className="control-label"
                    >
                      Sueldo Liquido
                    </label>
                    <small id="sueldoAyuda" className="form-text text-muted">
                      El sueldo que recibes luego de los descuentos legales.
                    </small>
                  </div>
                  <div className="form-group input-wrapper select">
                    <label htmlFor="AFP">
                      Indicanos tu AFP actual:
                      <select
                        className="form-control form-control-lg"
                        id="AFP"
                        name="AFP"
                        onChange={handleChange}
                      >
                        <option value="1">Seleccionar</option>
                        <option value="2">AFP Capital</option>
                        <option value="3">AFP Cuprum</option>
                        <option value="4">AFP Habitat</option>
                        <option value="5">AFP Plan Vital</option>
                        <option value="6">AFP Provida</option>
                        <option value="8">AFP Uno</option>
                      </select>
                    </label>
                  </div>
                  <div className="form-check">
                    <label className="form-check-label mt-auto" htmlFor="exampleCheck1">
                      <input type="checkbox" className="form-check-input" id="exampleCheck1" />
                      <small className="terminosYcondiciones">
                        Acepto las condiciones del servicio y la politica de privacidad.
                      </small>
                    </label>
                  </div>
                  <div className="d-flex justify-content-center">
                    <div className="col justify-content-center d-flex">
                      <button
                        type="submit"
                        className="btn btn-lg btn-block"
                      >
                        ¡Calcular!
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
      <Footer />
    </div>
  );
}
