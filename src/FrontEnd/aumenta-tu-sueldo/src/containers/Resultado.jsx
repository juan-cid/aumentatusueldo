import React from 'react';
import Header from '../components/Header';
import Footer from '../components/Footer';
import mujer from '../assets/svg/resultadoMina.svg';
import check from '../assets/svg/iconCheck.svg';
import rentabilidad from '../assets/svg/iconFinanzas.svg';
import comisiones from '../assets/svg/iconComisiones.svg';
import sac from '../assets/svg/iconServicio.svg';
import aumentarSueldo from '../assets/svg/mujerCerdito.svg';
import flecha from '../assets/svg/arrow-down.svg';
import mujerSAC from '../assets/svg/mujerSAC.svg';
import '../assets/styles/Resultado.css';

const Resultado = () => (
  <div className="resultado">
    <Header />
    <section>
      <div className="desktop">
        <div className="row w-100 container-padre aumentar">
          <div className="col-md-8 bloque desktop">
            <div className="txtDesktop">
              <div className="container-title">
                <h5>Tu sueldo líquido podría aumentar</h5>
                <h1 className="green">
                  $150.000
                  <span> pesos al año.</span>
                </h1>
                <p>
                  En AFP Modelo queremos ayudarte a tener una mejor pensión, para ello también nos encargamos
                  de ofrecerte una menor comisión por nuestros servicios. Lo que hace que tu sueldo líquido aumente
                  todos los meses. Puedes elegir pagar una menos comisión, puedes elegir ser parte de Modelo.
                </p>
                <button type="button" className="btn btn-lg btn-block">
                  Quiero cambiarme a Modelo
                </button>
              </div>
            </div>
          </div>
          <div className="col-md-4 bloque desktop">
            <img src={aumentarSueldo} alt="Aumenta tu sueldo" />
          </div>
        </div>
        <div className="row w-100 container-padre flecha">
          <img src={flecha} alt="down" />
        </div>
        <div className="row w-100 container-padre">
          <div className="txtDesktop">
            <p>Aumenta tu sueldo.</p>
          </div>
          <div className="txtDesktop">
            <p>Ahorrando en comisión.</p>
          </div>
          <div className="txtDesktop">
            <p>Haz tu traspaso 100% online.</p>
          </div>
        </div>
        <div className="row w-100 container-padre dudas">
          <div className="col-md-4 bloque desktop">
            <img src={mujerSAC} alt="Dudas" />
          </div>
          <div className="col-md-8 bloque desktop">
            <div className="txtDesktop">
              <div className="container-title">
                <h5>¿Aún tienes dudas?</h5>
                <p>
                  Nuestros ejecutivos pueden asesorarte en línea o vía teléfonica. Queremos ayudarte a resolver
                  todas tus inquietudes o darte todas las opciones para tu traspaso.
                </p>
                <button type="button" className="btn purple btn-lg btn-block">
                  Quiero que me contacten
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="row w-100 container-padre ofrecemos">
          <div className="col-12 d-flex justify-content-center m-5"><h5>¿Qué te ofrecemos?</h5></div>
          <div className="col-4">
            <div className="card">
              <div className="card-header beneficio">
                <img className="icon rentabilidad" src={rentabilidad} alt="rentabilidad" />
              </div>
              <div className="card-body">
                <p>La mejor rentabilitad en los fondos C, D y E en el último año.</p>
              </div>
            </div>
          </div>
          <div className="col-4">
            <div className="card">
              <div className="card-header beneficio">
                <img className="icon comisiones" src={comisiones} alt="comisiones" />
              </div>
              <div className="card-body">
                <p>Una de las comisiones más bajas del mercado 0,77%.</p>
              </div>
            </div>
          </div>
          <div className="col-4">
            <div className="card">
              <div className="card-header beneficio">
                <img className="icon sac" src={sac} alt="atención al cliente" />
              </div>
              <div className="card-body">
                <p>La mejor atención al cliente por 3 años consecutivos.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="mobile">
        <div className="row w-100 justify-content-center d-block">
          <div className="col-12 d-flex justify-content-center">
            <div className="container-title">
              <p>TU SUELDO AUMENTARÍA</p>
              <div className="sueldo">
                <h1>$150.560</h1>
                {' '}
                <p>al año.</p>
                <div className="row">
                  <div className="listado">
                    <div className="beneficio">
                      <img className="icon check" src={check} alt="check" />
                      <p>Aumenta tu sueldo.</p>
                    </div>
                    <div className="beneficio">
                      <img className="icon check" src={check} alt="check" />
                      <p>Ahorra en comisión.</p>
                    </div>
                    <div className="beneficio">
                      <img className="icon check" src={check} alt="check" />
                      <p>Traspasate 100% online.</p>
                    </div>
                    <button type="button" className="btn btn-lg btn-block">
                      Cambiarse a Modelo
                    </button>
                  </div>
                </div>
                <img className="mujer" src={mujer} alt="mujer" />
              </div>
              <div className="row">
                <div className="dudas">
                  <h5>¿Aún tienes dudas?</h5>
                  <p>
                    Nuestros ejecutivos pueden asesorarte en línea o vía teléfonica. Queremos ayudarte a resolver
                    todas tus inquietudes o darte todas las opciones para tu traspaso.
                  </p>
                  <button type="button" className="btn purple btn-lg btn-block">
                    Solicitar ejecutivo
                  </button>
                </div>
              </div>
              <div className="row">
                <div className="dudas ofrecemos">
                  <h5>¿Que te ofrecemos?</h5>
                  <div className="listado">
                    <div className="beneficio">
                      <img className="icon rentabilidad" src={rentabilidad} alt="rentabilidad" />
                      <p>A La mejor rentabilitad en los fondos D, C y E en los ultimos 3 años.</p>
                    </div>
                    <div className="beneficio">
                      <img className="icon comisiones" src={comisiones} alt="comisiones" />
                      <p>Una de las comisiones más bajas del mercado 0,77%.</p>
                    </div>
                    <div className="beneficio">
                      <img className="icon sac" src={sac} alt="servicio al cliente" />
                      <p>La mejor atención al cliente por 3 años consecutivos.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <Footer />
  </div>
);

export default Resultado;
