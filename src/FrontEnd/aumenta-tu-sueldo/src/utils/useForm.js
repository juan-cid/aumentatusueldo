import { useState } from 'react';

const useForm = (callback) => {
  const [form, setForm] = useState({
    nombre: '',
    rut: '',
    correo: '',
    celular: '',
    sueldo: '',
    isValid: false,
    AFPSelect: '',
  });
  /*
  const [errors, setErrors] = useState({

  }) */

  const handleChange = (e) => {
    e.preventDefault();

    const { name, value } = e.target;
    setForm({
      ...form,
      [name]: value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    callback();
  };

  return { form, handleChange, handleSubmit };
};

export default useForm;
