export const correoValidador = (email) => {
  let mensaje = '';
  let isValid = '';
  if (!email) {
    mensaje = 'Debe ingresar su correo';
    isValid = false;
  } else if (!/\S+@\S+\.\S{2,6}/.test(email)) {
    mensaje = 'Ingrese un correo valido';
    isValid = false;
  } else {
    isValid = true;
  }
  return { isValid, mensaje };
};

export const celularValidador = (celular) => {
  let mensaje = '';
  let isValid = false;

  if (!celular) {
    mensaje = 'Debe ingresar un numero de telefono';
  } else if (!/^[0-9]{9}$/.test(celular)) {
    mensaje = 'Ingrese un numero de telefono valido';
  }
  return { isValid, mensaje };
};

export const sueldoValidador = (sueldo) => {
  const RegExPattern = /[0-9]{6,9}$/;
  return RegExPattern.test(sueldo);
};

export const sueldoFormateador = (value) => {
  const formatterPeso = new Intl.NumberFormat('es-CL');
  return formatterPeso.format(value);
};
