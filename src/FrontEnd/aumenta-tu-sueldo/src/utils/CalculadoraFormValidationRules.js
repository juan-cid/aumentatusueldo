export default function validate(form) {
  let errors = {};

  if(!form.correoEmail) {
    errors.correoEmail = 'Debe ingresar su correo';
  } else if (!/\S+@\S+\.\S+/.test(form.correoEmail)) {
    errors.correoEmail = 'Ingrese un correo valido';
  }

  if(!form.celularNumber) {
    errors.celularNumber = 'Debe ingresar su numero de telefono';
  } else if (/^[0-9]{9}$/.test(form.celularNumber)) {
    errors.celularNumber = 'Ingrese un numero de telefono valido';
  }

  if(!form.sueldoNumber) {
    errors.sueldoNumber = 'Debe ingresar su sueldo líquido';
  } else if (/^[0-9]{6,9}$/.test(form.sueldoNumber)) {
    errors.sueldoNumber = 'Ingrese un sueldo mayor a $100.000';
  }

  return errors;
}
