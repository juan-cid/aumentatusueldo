import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import Calculadora from './containers/Calculadora';
import Resultado from './containers/Resultado';

const App = function () {
  return (
    <Router>
      <Switch>
        <Route path="/" exact component={Calculadora} />
        <Route path="/resultado" exact component={Resultado} />
      </Switch>
    </Router>
  );
};

export default App;
