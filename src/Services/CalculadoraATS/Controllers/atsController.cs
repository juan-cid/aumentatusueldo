﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using apiats.Models;
using Swashbuckle.AspNetCore.Annotations;



// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace apiats.Controllers
{
    [SwaggerResponse(200, "Todo OK")]
    [ApiController]
    [Route("api/[controller]")]
    public class atsController : ControllerBase
    {
       

        [HttpGet]
        // JSC: anotaciones con Swashbuckle.AspNetCore.Annotations
        [SwaggerOperation(Summary ="Get simple",Description = "Una api simple ...")]
        public IActionResult Get()
        {

            return Ok( new string[] { "",""});         
        }
        // JSC: Ejemplo de anotacion /// en Swagger 
        /// <summary>
        /// Esta api....
        /// </summary>
        /// <param name="rut"></param>
        /// <param name="nombre"></param>
        /// <param name="telefono"></param>
        /// <param name="correo"></param>
        /// <returns>OK si...</returns>
        [HttpGet("{rut}/{nombre}/{telefono}/{correo}")]
        public IActionResult Get(string rut,string nombre,string telefono,string correo)
        {
            DatosUsuario MiUsuario = new DatosUsuario();
            MiUsuario.Rut = rut;
            MiUsuario.Nombre = nombre;
            MiUsuario.Telefono = telefono;
            MiUsuario.Correo = correo;
            return Ok(MiUsuario);
            
        }

        // JSC : Probar datos de salida con estructuras o clases y con error
        [HttpPost("{rut}/{nombre}/{telefono}/{correo}")]
        [SwaggerResponse (404, "Parece que not found")]
        public ActionResult<DatosUsuario> Post(string rut, string nombre, string telefono, string correo)
        {
            DatosUsuario MiUsuario = new DatosUsuario();
            Random rng = new Random();
            // cada vez que salga par -> error
            if (rng.Next() % 2 == 0)
                return NotFound();

            MiUsuario.Rut = rut;
            MiUsuario.Nombre = nombre;
            MiUsuario.Telefono = telefono;
            MiUsuario.Correo = correo;
            return MiUsuario;

        }

    }
}
