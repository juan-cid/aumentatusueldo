﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using apiats.Models;
using Microsoft.VisualBasic;

namespace apiats.Controllers
{

    

    [ApiController]
    [Route("api/[controller]")]
    public class crentaliqController : Controller
    {
        public readonly  atsRepository  atsRepository;

        public crentaliqController()
        {
            atsRepository = new atsRepository();
        }

        [HttpGet]
        public IActionResult Get()
        {

            return Ok(new string[] { "", "" });
        }


        [HttpGet("{rentaliquida}/{afp}")]
       
        public ResultadoAts Get(double RentaLiquida,string Afp)
        {
            ResultadoAts resultadoAts= new ResultadoAts();
            IndicadoresParAFP IndicadoresAFPCliente = atsRepository.GetComisionAfp(Afp);
            IndicadoresParAFP IndicadoresAFPModelo = atsRepository.GetComisionAfp("Modelo");
            RentasTopes RentasTopes = atsRepository.GetSueldoTopeImponibles();

            IndicadoresPrev RangoISC =  atsRepository.GetbySueldo(RentaLiquida);

            IndicadoresPrev RangoISCSup = atsRepository.GetbyId(RangoISC.Id + 1);

            Double SueldoTributario = (RentaLiquida - RangoISC.CantidadRebajar) / (1 - RangoISC.Impuesto);

            Double SueldoTributario2 = (RentaLiquida - RangoISCSup.CantidadRebajar) / (1 - RangoISCSup.Impuesto);

            Double RentaTopeAFP = RentasTopes.Afp;
            Double RentaTopeSegCe = RentasTopes.SegCe;

            double SueldoBrutoAFPActual = 0;
            double ComisionAFpActual = 0;
            double ComisionAFpModelo = 0;
            double DiferenciaComision = 0;

            if (SueldoTributario>SueldoTributario2)
            {
                if (SueldoTributario <= RentaTopeAFP)
                {
                    ComisionAFpActual = SueldoTributario * (IndicadoresAFPCliente.ComisionAfp/100);
                    ComisionAFpModelo = SueldoTributario * (IndicadoresAFPModelo.ComisionAfp/100);
                    DiferenciaComision = ComisionAFpActual - ComisionAFpModelo;

                    //SueldoBrutoAFPActual = (SueldoTributario * (1.176)) + (SueldoTributario * (IndicadoresAFPCliente.ComisionAfp));

                }
                else
                {
                    ComisionAFpActual = RentaTopeAFP * (IndicadoresAFPCliente.ComisionAfp);
                    ComisionAFpModelo = RentaTopeAFP * (IndicadoresAFPModelo.ComisionAfp);
                    DiferenciaComision = ComisionAFpActual - ComisionAFpModelo;

                }
            
            }


            resultadoAts.SueldoAnual = Math.Round(DiferenciaComision) * 12;


            return resultadoAts;
        }
      
        
      
    }
}