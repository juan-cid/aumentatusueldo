﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiats.Models
{
    public class rentaliq
    {
        public string Moneda { get; set; }
        public Double RentaLiquida { get; set; }
        public Double RentaFinal => 32 + (Double)(RentaLiquida / 0.5556);
     
    }
}
