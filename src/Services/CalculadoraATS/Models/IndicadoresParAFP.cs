﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiats.Models
{
    public class IndicadoresParAFP
    {
        public string Afp { get; set; }
        public float TasaAfp { get; set; }
        public float ComisionAfp { get; set; }
    }
}
