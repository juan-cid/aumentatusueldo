﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiats.Models
{
    public class IndicadoresPrev
    {

        public int Id { get; set; }
        public float Impuesto { get; set; }
        public float CantidadRebajar { get; set; }

    }
}
