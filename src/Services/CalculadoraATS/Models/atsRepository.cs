﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;



namespace apiats.Models
{
    
    public class atsRepository

    {

        private string connectionString;

        public atsRepository()
        {
            connectionString = @"Data Source = 190.107.176.12,1433; Initial Catalog = aumentat_; User ID = DesaTI; Password = AdminTI2020;";
        }

        public IDbConnection Connection
        {
            get
            {
                return new SqlConnection(connectionString);
            }
        }

        public IndicadoresPrev GetbySueldo(double sueldo)
        {
            using (IDbConnection dbConnection = Connection)
            {
                string sQuery = "select id,Impuesto,CantidadRebajar,rentaliquidadesde,rentaliquidahasta from [aumentat_].[dbo].[IndicadoresISC] where RentaliquidaHasta  >= " + sueldo + " and RentaLiquidaDesde <= " + sueldo;
                dbConnection.Open();
                return dbConnection.Query<IndicadoresPrev>(sQuery).FirstOrDefault();
            }
        }


        public IndicadoresPrev GetbyId(int id)
        {
            using (IDbConnection dbConnection = Connection)
            {
                string sQuery = "select id,Impuesto,CantidadRebajar,rentaliquidadesde,rentaliquidahasta from [aumentat_].[dbo].[IndicadoresISC] where id  = " + id ;
                dbConnection.Open();
                return dbConnection.Query<IndicadoresPrev>(sQuery).FirstOrDefault();
            }
        }


        public IndicadoresParAFP GetComisionAfp(string afp)
        {
            using (IDbConnection dbConnection = Connection)
            {
                string sQuery = "select Afp,TasaAfp,ComisionAfp from [aumentat_].[dbo].[IndicadoresPrev] where Afp  = @Afp";
                dbConnection.Open();
                return dbConnection.Query<IndicadoresParAFP>(sQuery, new { Afp = afp }).FirstOrDefault();
            }
        }


        public RentasTopes GetSueldoTopeImponibles()
        {
            using (IDbConnection dbConnection = Connection)
            {
                string sQuery = "select Afp,SegCe from [aumentat_].[dbo].[SueldosTope]";
                dbConnection.Open();
                return dbConnection.Query<RentasTopes>(sQuery).FirstOrDefault();
            }
        }
    }
}
