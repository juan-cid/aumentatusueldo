﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiats.Models
{
    public class DatosUsuario
    {

        public string Rut { get; set; }
        public string Nombre { get; set; }
        //public string TemperatureF => 32 + (int)(TemperatureC / 0.5556);
        public string Correo { get; set; }
        public string Telefono { get; set; }
    }
}
