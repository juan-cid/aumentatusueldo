﻿using System;
using BackEnd_ATS.Application.Interfaces;
using BackEnd_ATS.Infrastructure.Repository;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;


namespace BackEnd_ATS.Infrastructure
{
    public static class ServiceRegistration
    {
        public static void AddInfrastructure(this IServiceCollection services)
        {

            services.AddTransient<IRepositorioAts_Prospectos, ATS_ProspectoRepositorio>();
            services.AddTransient<IAts_Prospectos, Ats_Prospectos>();
            services.AddTransient<IRepositorioAts_Simulacion, Ats_SimulacionRepositorio>();
            services.AddTransient<IAts_Simulacion, Ats_Simulacion>();
            services.AddTransient<IRepositorioSendrgridService,Ats_SendridServiceRepositorio>();
            services.AddTransient<ISendGridService,Ats_SendgridService>();

        }
    }
}
