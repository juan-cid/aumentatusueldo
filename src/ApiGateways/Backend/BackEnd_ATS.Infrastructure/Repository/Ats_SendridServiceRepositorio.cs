﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BackEnd_ATS.Application.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace BackEnd_ATS.Infrastructure.Repository
{
    public class Ats_SendridServiceRepositorio : IRepositorioSendrgridService
    {
        private readonly IServiceProvider services;

        public Ats_SendridServiceRepositorio(IServiceProvider service)
        {
            this.services = service;
        }
        public async Task<string> EnviarCorreoText(string mail_remitente,string remitente, string mail_receptor,string nombre,string asunto, string texto)
        {
            var client = this.services.GetRequiredService<ISendGridClient>();
            var msg = new SendGridMessage()
            {
                From = new EmailAddress(mail_remitente, remitente),
                Subject = asunto
            };
            msg.AddContent(MimeType.Text, texto);
            msg.AddTo(new EmailAddress(mail_receptor, nombre));
            var response = await client.SendEmailAsync(msg).ConfigureAwait(false);
            return response.Body.ToString();

        }

        public async Task<string> EnviarCorreoHTML(string mail_remitente, string remitente, string mail_receptor, string nombre, string asunto, string textoplano, string html)
        {
            var client = this.services.GetRequiredService<ISendGridClient>();
            var msg = new SendGridMessage()
            {
                From = new EmailAddress(mail_remitente,remitente),
                Subject = asunto,
                PlainTextContent = textoplano,
                HtmlContent = html
            };
            msg.AddTo(new EmailAddress(mail_receptor,nombre));
            var response = await client.SendEmailAsync(msg).ConfigureAwait(false);
            return response.Body.ToString();
        }
    }
}
