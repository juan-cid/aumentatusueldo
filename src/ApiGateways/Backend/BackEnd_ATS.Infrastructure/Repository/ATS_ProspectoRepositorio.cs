﻿using BackEnd_ATS.Application.Interfaces;
using BackEnd_ATS.Core.Entities;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_ATS.Infrastructure.Repository
{
    public class ATS_ProspectoRepositorio : IRepositorioAts_Prospectos
    {
        private readonly IConfiguration configuration;

        public ATS_ProspectoRepositorio(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public async Task<object> ActualizarAsync(ATS_Prospectos entidad)
        {

            string sql =
                "UPDATE ATS_Prospectos SET nombre = @nombre, rut = @rut, email = @email, sueldo = @sueldo, afp_origen = @afp_origen, cod_afp_origen=@cod_afp_origen, fono_fijo=@fono_fijo, fono_movil=@fono_movil, fec_ing_reg=@fec_ing_reg, canal=@canal, horario=@horario  WHERE email = @email AND rut=@rut";
            using (SqlConnection connection = new SqlConnection(configuration.GetConnectionString("aumentat_")))
            {
                connection.Open();
                int result = await connection.ExecuteAsync(sql, entidad);
                return new { status = 200, data = result }; 
            }
        }

        public Task<object> BorrarAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<int> BorrarAsync(string email)
        {
            string sql = "DELETE FROM ATS_Prospectos WHERE email = @email";
            using (SqlConnection connection = new SqlConnection(configuration.GetConnectionString("aumentat_")))
            {
                connection.Open();
                int result = await connection.ExecuteAsync(sql, new { email = email });
                return result;
            }
        }

        public async Task<IReadOnlyList<ATS_Prospectos>> ObtenerPorRut(string rut)
        {
            string sql = "SELECT * FROM ATS_Prospectos WHERE rut=@rut";
            using (SqlConnection connection = new SqlConnection(configuration.GetConnectionString("aumentat_")))
            {
                connection.Open();
                IEnumerable<ATS_Prospectos> result = await connection.QueryAsync<ATS_Prospectos>(sql);
                return result.ToList();
            }
        }

        public async Task<IReadOnlyList<ATS_Prospectos>> ObtenerPorRangoFechaIngreso(string desde, string hasta)
        {
            DateTime.TryParse(desde, out DateTime d);
            DateTime.TryParse(hasta, out DateTime h);
            string sql = "select nombre,rut,email,sueldo,afp_origen,cod_afp_origen,fono_fijo,fono_movil,fec_ing_reg,canal,horario from ATS_Prospectos";
            sql += " where CONVERT(DATE,(substring(fec_ing_reg,0,len(fec_ing_reg)-9)+'-'+substring(fec_ing_reg,5,len(fec_ing_reg)-12)+'-'+substring(fec_ing_reg,7,len(fec_ing_reg)-12))) ";
            sql += " BETWEEN @desde AND @hasta";
            using (SqlConnection connection = new SqlConnection(configuration.GetConnectionString("aumentat_")))
            {
                connection.Open();
                IEnumerable<ATS_Prospectos> result = await connection.QueryAsync<ATS_Prospectos>(sql, new { desde = d, hasta = h });
                return result.ToList();
            }
        }

        public async Task<IReadOnlyList<ATS_Prospectos>> ObtenerPorFechaIngreso(string fecha)
        {
            DateTime.TryParse(fecha, out DateTime d);
            string sql = "select nombre,rut,email,sueldo,afp_origen,cod_afp_origen,fono_fijo,fono_movil,fec_ing_reg,canal,horario from ATS_Prospectos";
            sql += " where CONVERT(DATE,(substring(fec_ing_reg,0,len(fec_ing_reg)-9)+'-'+substring(fec_ing_reg,5,len(fec_ing_reg)-12)+'-'+substring(fec_ing_reg,7,len(fec_ing_reg)-12)))=@fecha ";

            using (SqlConnection connection = new SqlConnection(configuration.GetConnectionString("aumentat_")))
            {
                connection.Open();
                IEnumerable<ATS_Prospectos> result = await connection.QueryAsync<ATS_Prospectos>(sql, new { fecha = fecha });
                return result.ToList();
            }
        }

        public async Task<object> GuardarAsync(ATS_Prospectos entidad)
        {
            entidad.fono_fijo = entidad.fono_movil;
            entidad.canal = "ATS";
            entidad.horario = "FH";
            string sql =
                "Insert into ATS_Prospectos(nombre,rut,email,sueldo,afp_origen,cod_afp_origen,fono_fijo,fono_movil,fec_ing_reg,canal,horario) VALUES (@nombre,@rut,@email,@sueldo,@afp_origen,@cod_afp_origen,@fono_fijo,@fono_movil,@fec_ing_reg,@canal,@horario)";
            try
            {
                using (SqlConnection connection = new SqlConnection(configuration.GetConnectionString("aumentat_")))
                {
                    connection.Open();
                    int result = await connection.ExecuteAsync(sql, entidad);
                    return new { status = 200, data = result };
                }
            }
            catch (Exception e)
            {

                return new {status = 415, data = $"{e.Message} \n {e?.InnerException?.Message}"};
            }
        }

        public Task<ATS_Prospectos> ObtenerPorIdAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<IReadOnlyList<ATS_Prospectos>> ObtenerTodosAsync()
        {
            string sql = "SELECT * FROM ATS_Prospectos";
            using (SqlConnection connection = new SqlConnection(configuration.GetConnectionString("aumentat_")))
            {
                connection.Open();
                IEnumerable<ATS_Prospectos> result = await connection.QueryAsync<ATS_Prospectos>(sql);
                return result.ToList();
            }
        }
    }
}
