﻿using System;
using System.Collections.Generic;
using System.Text;
using BackEnd_ATS.Application.Interfaces;

namespace BackEnd_ATS.Infrastructure.Repository
{
    public class Ats_SendgridService : ISendGridService
    {
        public Ats_SendgridService(IRepositorioSendrgridService service)
        {
            Mailer = service;
        }
        public IRepositorioSendrgridService Mailer { get; }
    }
}
