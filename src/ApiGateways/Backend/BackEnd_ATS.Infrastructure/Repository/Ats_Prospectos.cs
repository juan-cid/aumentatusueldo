﻿using BackEnd_ATS.Application.Interfaces;

namespace BackEnd_ATS.Infrastructure.Repository
{
    public class Ats_Prospectos : IAts_Prospectos
    {
        public Ats_Prospectos(IRepositorioAts_Prospectos repositorio_prospectos)
        {
            Prospectos = repositorio_prospectos;
        }
        public IRepositorioAts_Prospectos Prospectos { get; }
    }
}
