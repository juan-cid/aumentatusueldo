﻿using BackEnd_ATS.Application.Interfaces;

namespace BackEnd_ATS.Infrastructure.Repository
{
    public class Ats_Simulacion : IAts_Simulacion
    {
        public Ats_Simulacion(IRepositorioAts_Simulacion repositorio_simulacion)
        {
            Simulacion = repositorio_simulacion;
        }
        public IRepositorioAts_Simulacion Simulacion { get; }

       
    }
}
