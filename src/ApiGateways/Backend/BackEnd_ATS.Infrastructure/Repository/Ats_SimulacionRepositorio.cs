﻿using BackEnd_ATS.Application.Interfaces;
using BackEnd_ATS.Core.Entities;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using BackEnd_ATS.Lib;

namespace BackEnd_ATS.Infrastructure.Repository
{
    public class Ats_SimulacionRepositorio : IRepositorioAts_Simulacion
    {
        private readonly IConfiguration configuration;

        public Ats_SimulacionRepositorio(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public async Task<ATS_Simulacion> ObtenerPorIdAsync(int id)
        {
            string sql = "SELECT * FROM ATS_Simulacion WHERE id_simulacion = @id";
            using (SqlConnection connection = new SqlConnection(configuration.GetConnectionString("aumentat_")))
            {
                connection.Open();
                ATS_Simulacion result = await connection.QuerySingleOrDefaultAsync<ATS_Simulacion>(sql, new { id = id });
                return result;
            }
        }

        public async Task<IReadOnlyList<ATS_Simulacion>> ObtenerTodosAsync()
        {
            string sql = "SELECT * FROM ATS_Simulacion";
            using (SqlConnection connection = new SqlConnection(configuration.GetConnectionString("aumentat_")))
            {
                connection.Open();
                IEnumerable<ATS_Simulacion> result = await connection.QueryAsync<ATS_Simulacion>(sql);
                return result.ToList();
            }
        }

        public async Task<object> GuardarAsync(ATS_Simulacion entidad)
        {
            bool valido;
            valido = Funciones.RutValido(entidad.rut);
            valido = Funciones.CorreoValido(entidad.correo);
            if (valido == false) return new { status = 415, data = "rut ó formato de correo incorrecto!" };

            entidad.fecha_simulacion = DateTime.Now;
            
            string sql =
                "Insert into ATS_Usuarios(nombre,rut,correo,telefono_movil,sueldo,cod_afp_actual,fecha_simulacion,resultado) VALUES(@nombre,@rut,@correo,@telefono_movil,@sueldo,@cod_afp_actual,@fecha_simulacion,@resultado)";
            try
            {
                using (SqlConnection connection = new SqlConnection(configuration.GetConnectionString("aumentat_")))
                {
                    connection.Open();
                    int result = await connection.ExecuteAsync(sql, entidad);
                    return new { status = 200, data = result};
                }
            }
            catch (Exception e)
            {
                return new { status = 415, data = $"{e.Message} \n {e?.InnerException?.Message}" };
            }
        }

        public async Task<object> ActualizarAsync(ATS_Simulacion entidad)
        {
            throw new NotImplementedException();
        }

        public async Task<object> BorrarAsync(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<IReadOnlyList<ATS_Simulacion>> ObtenerSimulacionesPorFecha(DateTime desde, DateTime hasta)
        {
            string sql = "SELECT * FROM ATS_Simulacion where fecha_simulacion BETWEEN @desde AND @hasta";
            using (SqlConnection connection = new SqlConnection(configuration.GetConnectionString("aumentat_")))
            {
                connection.Open();
                IEnumerable<ATS_Simulacion> result = await connection.QueryAsync<ATS_Simulacion>(sql, new { desde = desde, hasta = hasta });
                return result.ToList();
            }
        }

        public async Task<IReadOnlyList<ATS_Simulacion>> ObtenerPorRut(string rut)
        {
            string sql = "SELECT * FROM ATS_Simulacion where rut=@rut ORDER BY fecha_simulacion DESC";
            using (SqlConnection connection = new SqlConnection(configuration.GetConnectionString("aumentat_")))
            {
                connection.Open();
                IEnumerable<ATS_Simulacion> result = await connection.QueryAsync<ATS_Simulacion>(sql, new { rut = rut });
                return result.ToList();
            }
        }
    }
}
