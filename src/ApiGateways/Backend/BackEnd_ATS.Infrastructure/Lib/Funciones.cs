﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace BackEnd_ATS.Lib
{
    public static class Funciones
    {
        public static string GenerateSHA512String(string inputString)
        {
            SHA512 sha512 = SHA512Managed.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(inputString);
            byte[] hash = sha512.ComputeHash(bytes);
            return GetStringFromHash(hash);
        }

        private static string GetStringFromHash(byte[] hash)
        {
            StringBuilder result = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("X2"));
            }
            return result.ToString();
        }

        public static bool CorreoValido(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                return false;
            }
            string expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(email, expresion))
            {
                if (Regex.Replace(email, expresion, string.Empty).Length == 0)
                {
                    return true;
                }
                return false;
            }
            return false;
        }

        public static bool FechaValida(string fecha)
        {
            DateTime minDate = DateTime.Parse("1000/12/28");
            DateTime maxDate = DateTime.Parse("9999/12/28");

            return DateTime.TryParse(fecha, out DateTime dt)
                   && dt <= maxDate
                   && dt >= minDate;
        }

        public static bool RutValido(string rut)
        {
            string[] run = rut.Split("-");
            if (!string.IsNullOrWhiteSpace(run[0]) && !string.IsNullOrWhiteSpace(run[1]))
            {
                string r = rut.Trim();
                int suma = 0;
                for (int x = r.Length - 1; x >= 0; x--)
                {
                    suma += int.Parse(char.IsDigit(r[x]) ? r[x].ToString(CultureInfo.InvariantCulture) : "0") *
                            ((r.Length - (x + 1)) % 6 + 2);
                }

                int numericDigito = 11 - suma % 11;
                string digito = numericDigito == 11
                    ? "0"
                    : numericDigito == 10 ? "K" : numericDigito.ToString(CultureInfo.InvariantCulture);
                return digito.ToLower() == run[1].Trim().ToLower();
            }
            return false;
        }

        public static bool SoloNumeros(string numero)
        {
                if (string.IsNullOrWhiteSpace(numero))
                {
                    return false;
                }
                string expresion;
                expresion = "0123456789";
                if (Regex.IsMatch(numero, expresion))
                {
                    if (Regex.Replace(numero, expresion, string.Empty).Length == 0)
                    {
                        return true;
                    }
                    return false;
                }
            return false;
            

        }


        //public static bool CheckChars(IEnumerable<char> str, string validchars)
        //{
        //    return str.All(x => validchars.Contains(x));
        //}
    }
}