﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BackEnd_ATS.Application.Interfaces
{
    public interface IRepositorioSendrgridService
    {
        public Task<string> EnviarCorreoText(string mail_remitente, string remitente, string mail_receptor, string nombre, string asunto, string texto);
        public Task<string> EnviarCorreoHTML(string mail_remitente, string remitente, string mail_receptor, string nombre, string asunto, string textoplano, string html);
    }
}
