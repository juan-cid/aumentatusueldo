﻿using BackEnd_ATS.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BackEnd_ATS.Application.Interfaces
{
    public interface IRepositorioAts_Prospectos : IRepositorioGenerico<ATS_Prospectos>
    {
        Task<int> BorrarAsync(string email);
        Task<IReadOnlyList<ATS_Prospectos>> ObtenerPorRut(string rut);
        Task<IReadOnlyList<ATS_Prospectos>> ObtenerPorRangoFechaIngreso(string desde, string hasta);
        Task<IReadOnlyList<ATS_Prospectos>> ObtenerPorFechaIngreso(string fecha);
    }
}
