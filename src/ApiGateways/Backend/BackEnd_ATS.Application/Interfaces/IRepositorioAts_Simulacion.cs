﻿using BackEnd_ATS.Core.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BackEnd_ATS.Application.Interfaces
{
    public interface IRepositorioAts_Simulacion : IRepositorioGenerico<ATS_Simulacion>
    {
        public Task<IReadOnlyList<ATS_Simulacion>> ObtenerSimulacionesPorFecha(DateTime desde, DateTime hasta);
        public Task<IReadOnlyList<ATS_Simulacion>> ObtenerPorRut(string rut);
    }
}
