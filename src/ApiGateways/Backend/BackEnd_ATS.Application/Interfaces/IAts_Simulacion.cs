﻿namespace BackEnd_ATS.Application.Interfaces
{
    public interface IAts_Simulacion
    {
        IRepositorioAts_Simulacion Simulacion { get; }
    }
}
