﻿using BackEnd_ATS.Core.Entities;
using System.Threading.Tasks;

namespace BackEnd_ATS.Application.Interfaces
{
    public interface IRepositorioAts_Usuarios : IRepositorioGenerico<Ats_Usuarios>
    {
        public Task<Ats_Usuarios> ObtenerUsuario(string username, string password);
    }
}
