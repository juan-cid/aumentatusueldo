﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BackEnd_ATS.Application.Interfaces
{
    public interface IRepositorioGenerico<T> where T : class
    {
        Task<T> ObtenerPorIdAsync(int id);
        Task<IReadOnlyList<T>> ObtenerTodosAsync();
        Task<object> GuardarAsync(T entidad);
        Task<object> ActualizarAsync(T entidad);
        Task<object> BorrarAsync(int id);
    }
}
