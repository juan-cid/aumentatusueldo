﻿namespace BackEnd_ATS.Application.Interfaces
{
    public interface IAts_Prospectos
    {
        IRepositorioAts_Prospectos Prospectos { get; }
    }
}
