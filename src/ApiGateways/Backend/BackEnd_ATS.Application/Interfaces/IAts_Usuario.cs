﻿namespace BackEnd_ATS.Application.Interfaces
{
    public interface IAts_Usuario
    {
        IRepositorioAts_Usuarios Usuarios { get; }
    }
}
