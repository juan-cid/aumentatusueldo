﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using ZendeskApi_v2;


namespace BackEnd_ATS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ZendDeskController : ControllerBase
    {
        private readonly IConfiguration configuration;

        public ZendDeskController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        [HttpPost]
        public IActionResult EnvioSendesk(string Motivo_Tag, string Email, string Nombre, string Fono, string Movil)
        {
            try
            {
                string url = "https://afpmodelo.zendesk.com/api/v2/";
                string user = "zendesk@afpmodelo.cl";
                string pass = "4fpm0d3l0";
                ZendeskApi zendeskClient = new ZendeskApi(url, user, pass);
                ZendeskApi_v2.Models.Users.GroupUserResponse requester = new ZendeskApi_v2.Models.Users.GroupUserResponse();

                requester = zendeskClient.Users.SearchByEmail(Email);
                if (requester.Count == 0)
                {
                    zendeskClient.Users.CreateUser(new ZendeskApi_v2.Models.Users.User() { Email = Email, Name = Email, Verified = false });
                }

                ZendeskApi_v2.Models.Tickets.Ticket newTicket = new ZendeskApi_v2.Models.Tickets.Ticket()
                {
                    Comment = new ZendeskApi_v2.Models.Tickets.Comment() { Public = true, Body = "Click to Call" },
                    Requester = new ZendeskApi_v2.Models.Tickets.Requester() { Email = Email },
                    Subject = Strings.Replace(Motivo_Tag, "_", " ")
                };
                newTicket.Tags = new List<string>
                {
                    Motivo_Tag
                };
                newTicket.Type = "incident";
                long.TryParse("382940964", out long subId);
                newTicket.SubmitterId = subId;
                newTicket.CustomFields = new List<ZendeskApi_v2.Models.Tickets.CustomField>
                {
                    new ZendeskApi_v2.Models.Tickets.CustomField() { Id = Conversions.ToLong("22060264"), Value = Nombre }, // Nombre del cliente 22060264
                    new ZendeskApi_v2.Models.Tickets.CustomField() { Id = Conversions.ToLong("22070734"), Value = Email }, // email
                    new ZendeskApi_v2.Models.Tickets.CustomField() { Id = Conversions.ToLong("22060294"), Value = Fono }, // fono
                    new ZendeskApi_v2.Models.Tickets.CustomField() { Id = Conversions.ToLong("22739134"), Value = Movil }, // Movil
                    new ZendeskApi_v2.Models.Tickets.CustomField() { Id = Conversions.ToLong("22070964"), Value = "Web-Click to Call" } // Canal
                };
                ZendeskApi_v2.Models.Tickets.IndividualTicketResponse ticketCreated = zendeskClient.Tickets.CreateTicket(newTicket);
                ticketCreated.Ticket.Status = "open";
                // mensaje1.InnerText = ticketCreated.Ticket.Id.ToString
                return Ok();
            }
            catch (Exception e)
            {

                return Ok($"{e.Message} \n {e.InnerException?.Message}");
            }
        }
    }
}
