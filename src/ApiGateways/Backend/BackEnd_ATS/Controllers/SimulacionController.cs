﻿using BackEnd_ATS.Application.Interfaces;
using BackEnd_ATS.Core.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BackEnd_ATS.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class SimulacionController : ControllerBase
    {
        private readonly IConfiguration configuration;
        private readonly IAts_Simulacion repoSimulacion;

        public SimulacionController(IConfiguration configuration, IAts_Simulacion simulacion)
        {
            this.configuration = configuration;
            repoSimulacion = simulacion;
        }



        
        [HttpGet]
        public async Task<IActionResult> ObtenerSimulacionesFecha(string desde, string hasta)
        {
            DateTime.TryParse(desde, out DateTime d);
            DateTime.TryParse(hasta, out DateTime h);
            IReadOnlyList<ATS_Simulacion> data = await repoSimulacion.Simulacion.ObtenerSimulacionesPorFecha(d, h);
            if (data == null)
            {
                return NotFound();
            }

            return Ok(data);
        }

        [HttpPost]
        public async Task<IActionResult> GuardarSimulacion([FromBody] ATS_Simulacion simulacion)
        {
            try
            {
                Task<object> data = repoSimulacion.Simulacion.GuardarAsync(simulacion);
                return Ok(data);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return NotFound(e.Message);
            }
        }

    }
}
