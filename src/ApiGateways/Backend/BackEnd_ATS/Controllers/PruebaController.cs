﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BackEnd_ATS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PruebaController : ControllerBase
    {
        static HttpClient client = new HttpClient();


        [HttpGet]
        public IActionResult ObtenerPorFecha()
        {

            //https://localhost:32772/weatherforecast
            System.Net.WebClient wc = new System.Net.WebClient();
            Uri URL = new Uri("https://localhost:32772/weatherforecast");
            string resultado = wc.DownloadString(URL);

                 
            return Ok(new string[] { "resultado", resultado });

        }


    }
}
