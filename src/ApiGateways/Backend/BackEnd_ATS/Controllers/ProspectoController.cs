﻿    using BackEnd_ATS.Application.Interfaces;
using BackEnd_ATS.Core.Entities;
using BackEnd_ATS.Lib;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd_ATS.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ProspectoController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IAts_Prospectos atsProspectos;

        public ProspectoController(IConfiguration configuration, IAts_Prospectos atsProspectos)
        {
            this.atsProspectos = atsProspectos;
            _configuration = configuration;
        }

        [HttpPost]
        public async Task<IActionResult> GuardarProspecto([FromBody] ATS_Prospectos prospecto)
        {

            if (Funciones.RutValido(prospecto.rut) != true && Funciones.CorreoValido(prospecto.email) != true)
            {
                return NoContent();
            }

            object result = await atsProspectos.Prospectos.GuardarAsync(prospecto);
            return Ok(result);

        }

        [Route("porfecha")]
        [HttpGet]
        public async Task<IActionResult> ObtenerPorFecha(string fecha)
        {
            if (string.IsNullOrEmpty(fecha))
            {
                return NoContent();
            }

            IReadOnlyList<ATS_Prospectos> data = await atsProspectos.Prospectos.ObtenerPorFechaIngreso(fecha);
            return Ok(data);

        }

        [Route("todos")]
        [HttpGet]
        public IActionResult ObtenerProspectos()
        {
            IReadOnlyList<ATS_Prospectos> result = atsProspectos.Prospectos.ObtenerTodosAsync().Result;
            if (result.Any())
            {
                string json = JsonConvert.SerializeObject(result);
                return Ok(json);
            }
            return NotFound();
        }


    }
}
