﻿using Newtonsoft.Json;
using System;

namespace BackEnd_ATS.Core.Entities
{
    public class ATS_Simulacion
    {
        [JsonProperty("nombre", Required = Required.Always)]
        public string nombre { get; set; }
        [JsonProperty("rut", Required = Required.Always)]
        public string rut { get; set; }
        [JsonProperty("correo", Required = Required.Always)]
        public string correo { get; set; }
        [JsonProperty("telefono_movil", Required = Required.Always)]
        public string telefono_movil { get; set; }
        [JsonProperty("sueldo", Required = Required.Always)]
        public double sueldo { get; set; }
        [JsonProperty("cod_afp_actual", Required = Required.Always)]
        public string cod_afp_actual { get; set; }
        [JsonProperty("fecha_simulacion", Required = Required.Always)]
        public DateTime fecha_simulacion { get; set; }
        [JsonProperty("resultado", Required = Required.Always)]
        public double resultado { get; set; }
    }
}
