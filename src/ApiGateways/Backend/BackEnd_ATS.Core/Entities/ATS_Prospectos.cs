﻿using Newtonsoft.Json;

namespace BackEnd_ATS.Core.Entities
{
    public class ATS_Prospectos
    {
        [JsonProperty("nombre", Required = Required.Always)]
        public string nombre { get; set; }
        [JsonProperty("rut", Required = Required.Always)]
        public string rut { get; set; }
        [JsonProperty("email", Required = Required.Default)]
        public string email { get; set; }
        [JsonProperty("sueldo", Required = Required.Default)]
        public string sueldo { get; set; }
        [JsonProperty("cod_afp_origen", Required = Required.Default)]
        public string cod_afp_origen { get; set; }
        [JsonProperty("fono_fijo", Required = Required.Default)]
        public string fono_fijo { get; set; }
        [JsonProperty("fono_movil", Required = Required.Default)]
        public string fono_movil { get; set; }
        [JsonProperty("fec_ing_reg", Required = Required.Default)]
        public string fec_ing_reg { get; set; }
        [JsonProperty("canal", Required = Required.Default)]
        public string canal { get; set; }
        [JsonProperty("horario", Required = Required.Default)]
        public string horario { get; set; }
    }
}
