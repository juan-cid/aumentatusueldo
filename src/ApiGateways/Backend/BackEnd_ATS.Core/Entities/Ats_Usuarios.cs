﻿using Newtonsoft.Json;
using System;

namespace BackEnd_ATS.Core.Entities
{
    public class Ats_Usuarios
    {
        [JsonProperty("id_usuario", Required = Required.AllowNull)]
        public int? id_usuario { get; set; }

        [JsonProperty("username", Required = Required.Always)]
        public string username { get; set; }

        [JsonProperty("password", Required = Required.Always)]
        public string password { get; set; }

        [JsonProperty("bearer_token", Required = Required.AllowNull)]
        public string bearer_token { get; set; }

        [JsonProperty("refresh_token", Required = Required.AllowNull)]
        public string refresh_token { get; set; }

        [JsonProperty("ultimo_ingreso", Required = Required.Default)]
        public DateTime ultimo_ingreso { get; set; }

        [JsonProperty("rut", Required = Required.AllowNull)]
        public string rut { get; set; }

    }
}
